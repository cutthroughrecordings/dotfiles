--[[ lvim is the global options object

Linters should be
filled in as strings with either
a global executable or a path to
an executable
]]

vim.cmd("setlocal tabstop=4 shiftwidth=4")

-- THESE ARE EXAMPLE CONFIGS FEEL FREE TO CHANGE TO WHATEVER YOU WANT
vim.opt.clipboard = "unnamedplus" -- allows neovim to access the system clipboard
vim.o.guifont = "FiraCode NF"
-- vim.cmd("let g:terminal_shell='pwsh'")
-- vim.cmd("set shell=\"C:\\Program Files\\PowerShell\\7\\pwsh.exe\"")
-- vim.cmd([[
-- set &terminal = 'C:\\Program Files\\PowerShell\\7\\pwsh.exe'
-- let &shell = 'C:\\Program Files\\PowerShell\\7\\pwsh.exe'
-- let &shellcmdflag = '-NoLogo -NoProfile -ExecutionPolicy RemoteSigned -Command [Console]::InputEncoding=[Console]::OutputEncoding=[System.Text.Encoding]::UTF8;'
-- let &shellredir = '-RedirectStandardOutput %s -NoNewWindow -Wait'
-- let &shellpipe = '2>&1 | Out-File -Encoding UTF8 %s; exit $LastExitCode'
-- set shellquote= shellxquote=
-- ]])

require("keybinds")

-- general
lvim.log.level = "warn"
lvim.format_on_save = false
lvim.colorscheme = "nightfox"
-- to disable icons and use a minimalist setup, uncomment the following
-- lvim.use_icons = false


-- TODO: User Config for predefined plugins
-- After changing plugin config exit and reopen LunarVim, Run :PackerInstall :PackerCompile
lvim.builtin.alpha.active = true
lvim.builtin.alpha.mode = "dashboard"
-- lvim.builtin.notify.active = true
lvim.builtin.terminal.active = true

lvim.builtin.nvimtree.setup.view.side = "left"
lvim.builtin.nvimtree.setup.renderer.icons.show.git = true

-- Additional Plugins
lvim.plugins = {
  -- { "folke/tokyonight.nvim" },
  {
    "folke/trouble.nvim",
    cmd = "TroubleToggle",
  },
  { 'vimwiki/vimwiki' },
  { 'EdenEast/nightfox.nvim' },
  { 'adelarsq/neofsharp.vim' },
  -- { 'hrsh7th/vim-vsnip' },
  { 'norcalli/nvim-colorizer.lua' },
  { 's1n7ax/nvim-search-and-replace' },
  { "tpope/vim-fugitive" },
  { 'stsewd/fzf-checkout.vim' },
  { "tommcdo/vim-exchange" },
  { "junegunn/vim-easy-align" },
  { 'theHamsta/nvim-dap-virtual-text' },
  -- { "rcarriga/nvim-dap-ui", requires = {"mfussenegger/nvim-dap"} },
  -- { '~/cmp-css-intellisense' }
  -- { 'rmagatti/auto-session'},
  { 'junegunn/fzf' },
  -- { 'hrsh7th/nvim-cmp' },
  { 'SirVer/ultisnips'},
  { "quangnguyen30192/cmp-nvim-ultisnips" },
  

  --   config = function()
  --     require("auto-session").setup {
  --       log_level = "error",
  --       auto_session_suppress_dirs = { "~/", "~/Projects", "~/Downloads", "/" },
  --     }
  --   end }
}
--
local cmp_ultisnips_mappings = require("cmp_nvim_ultisnips.mappings")
-- local cmp = require('cmp')
-- require("cmp").setup({
--   snippet = {
--     expand = function(args)
--       vim.fn["UltiSnips#Anon"](args.body)
--     end,
--   },
--   sources = {
--     { name = "ultisnips" },
--     -- more sources
--   },
--   -- recommended configuration for <Tab> people:
--   mapping = {
--     ["<Tab>"] = cmp.mapping(
--       function(fallback)
--         cmp_ultisnips_mappings.expand_or_jump_forwards(fallback)
--       end,
--       { "i", "s", --[[ "c" (to enable the mapping in command mode) ]] }
--     ),
--     ["<S-Tab>"] = cmp.mapping(
--       function(fallback)
--         cmp_ultisnips_mappings.jump_backwards(fallback)
--       end,
--       { "i", "s", --[[ "c" (to enable the mapping in command mode) ]] }
--     ),
--   },
-- })

-- require('luasnip/loaders/from_vscode').lazy_load({path = '~/.config/lvim/snippets'})

-- require("auto-session").setup {
--   log_level = "error",
--   auto_session_suppress_dirs = { "~/", "~/Projects", "~/Downloads", "/" },
--   -- auto_restore_enabled = false
-- }

require("lspsettings")

require("nvim-dap-virtual-text").setup()
require("dapui").setup()
local dap, dapui = require("dap"), require("dapui")
dap.listeners.after.event_initialized["dapui_config"] = function()
  dapui.open()
end
dap.listeners.before.event_terminated["dapui_config"] = function()
  dapui.close()
end
dap.listeners.before.event_exited["dapui_config"] = function()
  dapui.close()
end

require 'colorizer'.setup {
  -- 'css';
  -- 'scss';
  -- 'javascript';
  'fsharp';
  -- 'lua';
  -- 'vim';
  -- 'html';
}

require 'nvim-search-and-replace'.setup()

-- Autocommands (https://neovim.io/doc/user/autocmd.html)
-- vim.api.nvim_create_autocmd("BufEnter", {
--   pattern = { "*.json", "*.jsonc" },
--   -- enable wrap mode for json files only
--   command = "setlocal wrap",
-- })
-- vim.api.nvim_create_autocmd("FileType", {
--   pattern = "zsh",
--   callback = function()
--     -- let treesitter use bash highlight for zsh files as well
--     require("nvim-treesitter.highlight").attach(0, "bash")
--   end,
-- })


require("wslsettings")

require("lualineconfig")

-- Temporary change until LVIM updates to fix this
lvim.builtin.bufferline.options.indicator_icon = nil
lvim.builtin.bufferline.options.indicator = { style = "icon", icon = "▎" }

vim.cmd([[

let g:vimwiki_list = [{'path': '~/vimwiki', 'template_path': '~/vimwiki/templates/',
          \ 'template_default': 'default', 'syntax': 'markdown', 'ext': '.md',
          \ 'path_html': '~/vimwiki/site_html/', 'custom_wiki2html': 'vimwiki_markdown',
          \ 'html_filename_parameterization': 1,
          \ 'template_ext': '.tpl'}]
]])

-- let g:vimwiki_list = [{'path': '~/vimwiki/',
--                       \ 'syntax': 'markdown', 'ext': '.md',
--                       \ 'custom_wiki2html': 'vimwiki_markdown'}]
