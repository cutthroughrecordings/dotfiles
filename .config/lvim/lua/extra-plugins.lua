-- Additional Plugins
lvim.plugins = {
  -- { "folke/tokyonight.nvim" },
  {
    "folke/trouble.nvim",
    cmd = "TroubleToggle",
  },
  { 'vimwiki/vimwiki' },
  { 'EdenEast/nightfox.nvim' },
  { "lukas-reineke/indent-blankline.nvim" },

  { "tpope/vim-fugitive"},
  -- { '~/cmp-css-intellisense' }
  -- { 'rmagatti/auto-session',
  --   config = function()
  --     require("auto-session").setup {
  --       log_level = "error",
  --       auto_session_suppress_dirs = { "~/", "~/Projects", "~/Downloads", "/" },
  --     }
  --   end }
}

vim.opt.list = true

require("indent_blankline").setup {
  space_char_blankline = " ",
  show_current_context = true,
  show_current_context_start = true,
}
