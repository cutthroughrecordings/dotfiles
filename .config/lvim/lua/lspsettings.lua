-- if you don't want all the parsers change this to a table of the ones you want
lvim.builtin.treesitter.ensure_installed = {
  "bash",
  "c",
  "javascript",
  "json",
  "lua",
  "python",
  "typescript",
  "tsx",
  "css",
  "rust",
  "java",
  "yaml",
  "vue"
}

lvim.builtin.treesitter.ignore_install = { "haskell" }
lvim.builtin.treesitter.highlight.enabled = true

-- generic LSP settings

-- -- make sure server will always be installed even if the server is in skipped_servers list
-- lvim.lsp.installer.setup.ensure_installed = {
--     "sumeko_lua",
--     "jsonls",
-- }
-- -- change UI setting of `LspInstallInfo`
-- -- see <https://github.com/williamboman/nvim-lsp-installer#default-configuration>
-- lvim.lsp.installer.setup.ui.check_outdated_servers_on_open = false
-- lvim.lsp.installer.setup.ui.border = "rounded"
-- lvim.lsp.installer.setup.ui.keymaps = {
--     uninstall_server = "d",
--     toggle_server_expand = "o",
-- }

-- ---@usage disable automatic installation of servers
-- lvim.lsp.automatic_servers_installation = false

-- ---configure a server manually. !!Requires `:LvimCacheReset` to take effect!!
-- ---see the full default list `:lua print(vim.inspect(lvim.lsp.automatic_configuration.skipped_servers))`

vim.list_extend(lvim.lsp.automatic_configuration.skipped_servers, { "tailwindcss" })
local opts = {} -- check the lspconfig documentation for a list of all possible options
require("lvim.lsp.manager").setup("tailwindcss", opts)

lvim.builtin.dap.active = true

local dap = require('dap')

dap.adapters.lldb = {
  type = 'executable',
  command = '/usr/bin/lldb-vscode', -- adjust as needed, must be absolute path
  name = 'lldb'
}

dap.configurations.d = {
{
    name = 'Launch',
    type = 'lldb',
    request = 'launch',
    program = function()
      return vim.fn.input('Path to executable: ', vim.fn.getcwd() .. '/', 'file')
    end,
    cwd = '${workspaceFolder}',
    stopOnEntry = false,
    args = {}
  }
}
dap.configurations.cpp = dap.configurations.d
dap.configurations.c = dap.configurations.d
dap.configurations.rust = dap.configurations.d

-- dap.adapters.firefox = {
--   type = 'executable',
--   command = 'node',
--   args = { '/home/reker/daps/vscode-firefox-debug/dist/adapter.bundle.js' },
-- }

dap.adapters.chrome = {
  type = "executable",
  command = "node",
  args = { "C:/Users/ethanpreker/.debuggers/vscode-chrome-debug/out/src/chromeDebug.js" } -- TODO adjust
}
dap.adapters.vue = {
  type = "executable",
  command = "node",
  args = { "C:/Users/ethanpreker/.debuggers/vscode-chrome-debug/out/src/chromeDebug.js" } -- TODO adjust
}

local sourceMapPathOverrides = {}
sourceMapPathOverrides["webpack://checksmith/./src/*"] = "${webRoot}/src/*"

dap.configurations.vue = { -- change to typescript if needed
  
  {
    type = "chrome",
    request = "launch",
    name = "Vue CLI v5: Chrome",
    url = "http://localhost:8080",
    webRoot = "${workspaceFolder}",
    sourceMapPathOverrides = sourceMapPathOverrides
  }
}

-- Corresponding vue.config.js
-- const fs = require('fs');
-- const { SourceMapConsumer, SourceMapGenerator } = require('source-map');

-- module.exports = {
--   configureWebpack() {
--     return {
--       devtool: 'source-map',
--       plugins: [{
--         apply(compiler) {
--           compiler.hooks.thisCompilation.tap('Initializing Compilation', (compilation) => {
--             compilation.hooks.finishModules.tapPromise('All Modules Built', async (modules) => {
--               for (const module of modules) {
--                 if (shouldSkipModule(module)) continue;

--                 const pathWithoutQuery = module.resource.replace(/\?.*$/, '');
--                 const sourceFile = fs.readFileSync(pathWithoutQuery).toString('utf-8');
--                 const sourceMap = extractSourceMap(module);

--                 sourceMap.sources = [pathWithoutQuery];
--                 sourceMap.sourcesContent = [sourceFile];
--                 sourceMap.mappings = await shiftMappings(sourceMap, sourceFile, pathWithoutQuery);
--               }
--             });
--           });
--         }
--       }]
--     };
--   }
-- };

-- function shouldSkipModule(module) {
--   const { resource = '' } = module;

--   if (!resource) return true;
--   if (/node_modules/.test(resource)) return true;
--   if (!/\.vue/.test(resource)) return true;
--   if (!/type=script/.test(resource)) return true;
--   if (!/lang=ts/.test(resource)) return true;
--   if (isMissingSourceMap(module)) return true;

--   return false;
-- }

-- function isMissingSourceMap(module) {
--   return !extractSourceMap(module);
-- }

-- function extractSourceMap(module) {
--   if (!module['_source']) return null;

--   return module['_source']['_sourceMap'] ||
--     module['_source']['_sourceMapAsObject'] ||
--     null;
-- }

-- async function shiftMappings(sourceMap, sourceFile, sourcePath) {
--   const indexOfScriptTag = getIndexOfScriptTag(sourceFile);

--   const shiftedSourceMap = await SourceMapConsumer.with(sourceMap, null, async (consumer) => {
--     const generator = new SourceMapGenerator();

--     consumer.eachMapping((mapping) => {
--       const {
--         generatedColumn,
--         generatedLine,
--         originalColumn,
--         originalLine
--       } = mapping;

--       let name = mapping.name;
--       let source = sourcePath;

--       if (originalLine === null || originalColumn === null) {
--         name = null;
--         source = null;
--       }
--       else {
--         original = {
--           column: originalColumn,
--           line: originalLine + indexOfScriptTag,
--         };
--       }

--       generator.addMapping({
--         generated: {
--           column: generatedColumn,
--           line: generatedLine,
--         },
--         original,
--         source,
--         name
--       });
--     });

--     return generator.toJSON();
--   });

--   return shiftedSourceMap.mappings;
-- }

-- function getIndexOfScriptTag(sourceFile) {
--   const lines = sourceFile.match(/.+/g);
--   let indexOfScriptTag = 0;

--   for (const line of lines) {
--     ++indexOfScriptTag;
--     if (/<script/.test(line)) break;
--   }

--   return indexOfScriptTag;
-- }


-- vim.list_extend(lvim.lsp.automatic_configuration.skipped_servers, { "pyright" })
-- local opts = {} -- check the lspconfig documentation for a list of all possible options
-- require("lvim.lsp.manager").setup("pyright", opts)

-- ---remove a server from the skipped list, e.g. eslint, or emmet_ls. !!Requires `:LvimCacheReset` to take effect!!
-- ---`:LvimInfo` lists which server(s) are skipped for the current filetype
-- vim.tbl_map(function(server)
--   return server ~= "emmet_ls"
-- end, lvim.lsp.automatic_configuration.skipped_servers)

-- you can set a custom on_attach function that will be used for all the language servers
-- See <https://github.com/neovim/nvim-lspconfig#keybindings-and-completion>
lvim.lsp.on_attach_callback = function(client, bufnr)
  local function buf_set_option(...)
    vim.api.nvim_buf_set_option(bufnr, ...)
  end

  -- You will likely want to reduce updatetime which affects CursorHold
  -- note: this setting is global and should be set only once
  vim.o.updatetime = 250
  vim.cmd [[autocmd! CursorHold,CursorHoldI * lua vim.diagnostic.open_float(nil, {focus=false})]]

  --Enable completion triggered by <c-x><c-o>
  buf_set_option("omnifunc", "v:lua.vim.lsp.omnifunc")
end

-- -- set a formatter, this will override the language server formatting capabilities (if it exists)
-- local formatters = require "lvim.lsp.null-ls.formatters"
-- formatters.setup {
--   { command = "black", filetypes = { "python" } },
--   { command = "isort", filetypes = { "python" } },
--   {
--     -- each formatter accepts a list of options identical to https://github.com/jose-elias-alvarez/null-ls.nvim/blob/main/doc/BUILTINS.md#Configuration
--     command = "prettier",
--     ---@usage arguments to pass to the formatter
--     -- these cannot contain whitespaces, options such as `--line-width 80` become either `{'--line-width', '80'}` or `{'--line-width=80'}`
--     extra_args = { "--print-with", "100" },
--     ---@usage specify which filetypes to enable. By default a providers will attach to all the filetypes it supports.
--     filetypes = { "typescript", "typescriptreact" },
--   },
-- }

-- -- set additional linters
-- local linters = require "lvim.lsp.null-ls.linters"
-- linters.setup {
--   { command = "flake8", filetypes = { "python" } },
--   {
--     -- each linter accepts a list of options identical to https://github.com/jose-elias-alvarez/null-ls.nvim/blob/main/doc/BUILTINS.md#Configuration
--     command = "shellcheck",
--     ---@usage arguments to pass to the formatter
--     -- these cannot contain whitespaces, options such as `--line-width 80` become either `{'--line-width', '80'}` or `{'--line-width=80'}`
--     extra_args = { "--severity", "warning" },
--   },
--   {
--     command = "codespell",
--     ---@usage specify which filetypes to enable. By default a providers will attach to all the filetypes it supports.
--     filetypes = { "javascript", "python" },
--   },
-- }

-- local cmp = require 'cmp'
-- cmp.setup({
--   cmp.setup.filetype('vue', {
--     sources = cmp.config.sources({
--       { name = 'css_intellisense' }, -- You can specify the `cmp_git` source if you were installed it.
--     }, {
--       { name = 'buffer' }, -- You can specify the `cmp_git` source if you were installed it.
--     }, {
--       { name = 'path' }, -- Ynvim_lsp:volar' }, -- You can specify the `cmp_git` source if you were installed it.
--     })
--   })
-- })
