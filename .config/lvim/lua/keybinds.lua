-- Vimwiki toggle list items
vim.api.nvim_set_keymap("n", "<Space>t", ":VimwikiToggleListItem<cr>",
  { silent = true, noremap = true }
)

vim.api.nvim_set_keymap("n", "<M>t", ":ToggleTerm<cr>",
  { silent = true, noremap = true }
)

-- vim.api.nvim_set_keymap("n", "<M>w", ":if &modifiable && !&readonly && &modified <CR> :write<CR> :endif<CR>:bnext<CR>",
--   { silent = true, noremap = true }
-- )
-- vim.api.nvim_set_keymap("n", "<M>q", ":if &modifiable && !&readonly && &modified <CR> :write<CR> :endif<CR>:bprevious<CR>",
--   { silent = true, noremap = true }
-- )
vim.cmd([[
nnoremap  <silent>   <tab>  :if &modifiable && !&readonly && &modified <CR> :write<CR> :endif<CR>:bnext<CR>
nnoremap  <silent> <s-tab>  :if &modifiable && !&readonly && &modified <CR> :write<CR> :endif<CR>:bprevious<CR>
]])

-- keymappings [view all the defaults by pressing <leader>Lk]
lvim.leader = "space"
-- add your own keymapping
lvim.keys.normal_mode["<C-s>"] = ":wcr>"
-- lvim.keys.normal_mode["<C-/>"] = ":gcc"
-- unmap a default keymapping
-- vim.keymap.del("n", "<C-Up>")
-- override a default keymapping
-- lvim.keys.normal_mode["<C-q>"] = ":q<cr>" -- or vim.keymap.set("n", "<C-q>", ":q<cr>" )

-- Change Telescope navigation to use j and k for navigation and n and p for history in both input and normal mode.
-- we use protected-mode (pcall) just in case the plugin wasn't loaded yet.
-- local _, actions = pcall(require, "telescope.actions")
-- lvim.builtin.telescope.defaults.mappings = {
--   -- for input mode
--   i = {
--     ["<C-j>"] = actions.move_selection_next,
--     ["<C-k>"] = actions.move_selection_previous,
--     ["<C-n>"] = actions.cycle_history_next,
--     ["<C-p>"] = actions.cycle_history_prev,
--   },
--   -- for normal mode
--   n = {
--     ["<C-j>"] = actions.move_selection_next,
--     ["<C-k>"] = actions.move_selection_previous,
--   },
-- }

vim.api.nvim_set_keymap('n', '<C-_>', 'gcc', { noremap = false })
vim.api.nvim_set_keymap('v', '<C-_>', 'gcc', { noremap = false })

-- Use which-key to add extra bindings with the leader-key prefix
lvim.builtin.which_key.mappings["P"] = { "<cmd>Telescope projects<CR>", "Projects" }
lvim.builtin.which_key.mappings["F"] = { "<cmd>only<CR>", "Focus Current Buffer" }
lvim.builtin.which_key.mappings["t"] = {
  name = "+Trouble",
  r = { "<cmd>Trouble lsp_references<cr>", "References" },
  f = { "<cmd>Trouble lsp_definitions<cr>", "Definitions" },
  d = { "<cmd>Trouble document_diagnostics<cr>", "Diagnostics" },
  q = { "<cmd>Trouble quickfix<cr>", "QuickFix" },
  l = { "<cmd>Trouble loclist<cr>", "LocationList" },
  w = { "<cmd>Trouble workspace_diagnostics<cr>", "Wordspace Diagnostics" },
}
-- Custom Git Mappings
lvim.builtin.which_key.mappings["g"] = {
  name = "+Git",
  o = { "<cmd>Git<cr>", "Open Git" },
  c = { "<cmd>close<cr>", "Close Git" },
  r = { "<cmd>Gread<cr>", "Reset file" },
  d = { "<cmd>Gvdiffsplit<cr>", "Diff current file" },
  b = { "<cmd>Telescope git_branches<CR>", "Branches" },
  m = {
    name = "+Merge",
    o = { "<cmd>Gvdiffsplit!<cr>", "Resolve Merge Conflicts" },
    t = { "<cmd>diffget //2<cr>", "Take Target" },
    s = { "<cmd>diffget //3<cr>", "Take Source" }
  }
}

-- Custom C#/Vb.Net Mappings
lvim.builtin.which_key.mappings["V"] = {
  name = "+Visual Studio",
  i = {
    name = "+Increment",
    p = { "<cmd>!python ../increment-versions.py --dir ./ --patch<cr>", "Patch" },
    m = { "<cmd>!python ../increment-versions.py --dir ./ --minor<cr>", "Minor" },
    M = { "<cmd>!python ../increment-versions.py --dir ./ --major<cr>", "Major" }
  }
}
lvim.autocommands = {
  {
    "BufWinEnter", {
      pattern = { "*.d", "dub.json", "plugin.json", "dub.selections.json" },
      callback = function()
        local wk = require("which-key")
        wk.register({
          D = {
            name = "+D", -- optional group name
            a = {'<cmd>exec ":!dub add ".input("Package:")<cr>', 'Add Package'},
            c = {'<cmd>!dub clean<cr>', 'Clean Project'},
            p = {
              name = "+Dplug",
              b = { "<cmd>!dplug-build -c VST3<cr>", "Build" },
            }
          },
        }, { prefix = "<leader>" }) -- }, opts)
      end
    },
  },
  -- {
  --   "ExitPre", {
  --     pattern = { "*" },
  --     callback = function()
  --       vim.cmd('NvimTreeClose')
  --       -- nvtree = require('nvim-tree')
  --       -- nvtree.close()
  --     end
  --   },
  -- }
}
