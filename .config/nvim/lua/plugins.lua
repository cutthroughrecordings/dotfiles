return require('packer').startup(function()
  use 'wbthomason/packer.nvim' -- so packer can update itself
  use 'neovim/nvim-lspconfig'
  use 'hrsh7th/cmp-nvim-lsp'
  use 'hrsh7th/cmp-buffer'
  use 'hrsh7th/cmp-path'
  use 'hrsh7th/cmp-cmdline'
  use 'hrsh7th/nvim-cmp'

  use 'nanotee/sqls.nvim'

  -- Fsharp
  use 'adelarsq/neofsharp.vim'
  use 'ionide/Ionide-vim'
  use { "williamboman/mason.nvim" }
  use {
    "williamboman/mason.nvim",
    "williamboman/mason-lspconfig.nvim",
  }
end)
