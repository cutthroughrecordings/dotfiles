-- Add additional capabilities supported by nvim-cmp
local capabilities = vim.lsp.protocol.make_client_capabilities()
capabilities = require('cmp_nvim_lsp').update_capabilities(capabilities)

require("nvim-lsp-installer").setup()

local function on_attach(client, bufnr)
    -- set up buffer keymaps, etc.
end

require("mason").setup()
require("mason-lspconfig").setup()

local lspconfig = require('lspconfig')

--local servers = {
  --'vuels',
  --local servers = {
  --'tsserver',
  --'serve_d',
  --'pyright',
  --'lemminx',
  --'hls',
  --'gopls',
  --'omnisharp',
  --'clangd',
  --'asm_lsp',
  --'ltex',
  --'marksman',
  --'solang'
--}

for _, lsp in ipairs(servers) do
	lspconfig[lsp].setup {
    capabilities = capabilities,
    on_attach = on_attach,
  }
end

lspconfig.sqls.setup{
  capabilities = capabilities,
  on_attach = function(client, bufnr)
        require('sqls').on_attach(client, bufnr)
  end,
  settings = {
    sqls = {
      connections = {
        {
          driver = 'mssql',
          dataSourceName = 'Data Source=PF23FNFR.wwprogramming.com\\acct;Initial Catalog=Employee;User=SqlsUser;Password=12348765;',
        },
      },
    },
  },
}
--lspconfig.sqls.setup{
--  on_attach = function(client, bufnr)
--      require('sqls').on_attach(client, bufnr)
--  end
--}

vim.cmd [[
autocmd BufNewFile,BufRead *.fs,*.fsx,*.fsi set filetype=fsharp
]]

lspconfig.sumneko_lua.setup {
  settings = {
    Lua = {
      runtime = {
        -- Tell the language server which version of Lua you're using (most likely LuaJIT in the case of Neovim)
        version = 'LuaJIT',
      },
      diagnostics = {
        -- Get the language server to recognize the `vim` global
        globals = {'vim'},
      },
      workspace = {
        -- Make the server aware of Neovim runtime files
        library = vim.api.nvim_get_runtime_file("", true),
      },
      -- Do not send telemetry data containing a randomized but unique identifier
      telemetry = {
        enable = false,
      },
    },
  },
  capabilities = capabilities
}

-- luasnip setup
local luasnip = require 'luasnip'

-- nvim-cmp setup
local cmp = require 'cmp'
cmp.setup {
  snippet = {
    expand = function(args)
      luasnip.lsp_expand(args.body)
    end,
  },
  mapping = cmp.mapping.preset.insert({
    ['<C-d>'] = cmp.mapping.scroll_docs(-4),
    ['<C-f>'] = cmp.mapping.scroll_docs(4),
    ['<C-Space>'] = cmp.mapping.complete(),
    ['<CR>'] = cmp.mapping.confirm {
      behavior = cmp.ConfirmBehavior.Replace,
      select = true,
    },
    ['<Tab>'] = cmp.mapping(function(fallback)
      if cmp.visible() then
        cmp.select_next_item()
      elseif luasnip.expand_or_jumpable() then
        luasnip.expand_or_jump()
      else
        fallback()
      end
    end, { 'i', 's' }),
    ['<S-Tab>'] = cmp.mapping(function(fallback)
      if cmp.visible() then
        cmp.select_prev_item()
      elseif luasnip.jumpable(-1) then
        luasnip.jump(-1)
      else
        fallback()
      end
    end, { 'i', 's' }),
  }),
  sources = {
    { name = 'nvim_lsp' },
    { name = 'luasnip' },
  },
}
