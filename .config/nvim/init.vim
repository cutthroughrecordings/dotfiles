source ~/.vimrc

if has('nvim')
lua <<EOF
require('config')
EOF
endif
