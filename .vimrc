set encoding=UTF-8
set number
set relativenumber
set mouse=a
set ignorecase
set clipboard=unnamed

nnoremap <SPACE> <Nop>
let mapleader=" "

nnoremap  <silent>   <tab>  :if &modifiable && !&readonly && &modified <CR> :write<CR> :endif<CR>:bnext<CR>
nnoremap  <silent> <s-tab>  :if &modifiable && !&readonly && &modified <CR> :write<CR> :endif<CR>:bprevious<CR>

call plug#begin()
Plug 'aluriak/nerdcommenter'

Plug 'dracula/vim', { 'as': 'dracula' }

Plug 'ncm2/ncm2'
Plug 'honza/vim-snippets'

Plug 'vimwiki/vimwiki'


Plug 'tpope/vim-fugitive'

Plug 'sheerun/vim-polyglot'

Plug 'ryanoasis/vim-devicons'
Plug 'preservim/nerdtree' |
            \ Plug 'Xuyuanp/nerdtree-git-plugin'

Plug 'vim-airline/vim-airline'
Plug 'vim-airline/vim-airline-themes'

Plug 'BurntSushi/ripgrep'
Plug 'sharkdp/fd'

Plug 'matze/vim-move'

" NeoVim Specific Plugins
if has('nvim')
    Plug 'kyazdani42/nvim-web-devicons' " optional, for file icons

    Plug 'hrsh7th/cmp-nvim-lsp'
    Plug 'saadparwaiz1/cmp_luasnip'
    Plug 'L3MON4D3/LuaSnip'

    Plug 'folke/lsp-colors.nvim'

    Plug 'EdenEast/nightfox.nvim'
    Plug 'lukas-reineke/indent-blankline.nvim'

    Plug 'nanotee/sqls.nvim'

    Plug 'windwp/nvim-autopairs'

    Plug 'nvim-lua/plenary.nvim'
    Plug 'nvim-telescope/telescope.nvim', { 'tag': '0.1.0' }
    Plug 'nvim-telescope/telescope-fzf-native.nvim', { 'do': 'cmake -S. -Bbuild -DCMAKE_BUILD_TYPE=Release && cmake --build build --config Release && cmake --install build --prefix build' }

    Plug 'folke/trouble.nvim'
    Plug 'kyazdani42/nvim-web-devicons'

    " Cosmic UI
    Plug 'MunifTanjim/nui.nvim'
    Plug 'CosmicNvim/cosmic-ui'

    Plug 'nvim-treesitter/nvim-treesitter'

    Plug 'kyazdani42/nvim-web-devicons'
    Plug 'nvim-lua/plenary.nvim'
    Plug 'nvim-telescope/telescope.nvim', { 'tag': '0.1.0' }
endif
call plug#end()

" Nightfox theme only available in nvim
if has('nvim')
  colorscheme nightfox
else
  colorscheme dracula
endif

" Vimwiki toggle list items
nnoremap <leader>t :VimwikiToggleListItem<CR>

" Load Nerdtree config
source $HOME/.config/nvim/nerdtree.vim

" Set up telescope shortcuts
nnoremap <leader>ff <cmd>lua require('telescope.builtin').find_files()<cr>
nnoremap <leader>fg <cmd>lua require('telescope.builtin').live_grep()<cr>
nnoremap <leader>fb <cmd>lua require('telescope.builtin').buffers()<cr>
nnoremap <leader>fh <cmd>lua require('telescope.builtin').help_tags()<cr>

" Binds CNTRL h,j,k,l to switching panes
map  <C-j> <C-W>j
map  <C-k> <C-W>k
map  <C-h> <C-W>h
map  <C-l> <C-W>l
" Needed for terminals
tmap <C-j> <C-\><C-n><C-w>j
tmap <C-h> <C-\><C-n><C-w>h
tmap <C-k> <C-\><C-n><C-w>k
tmap <C-l> <C-\><C-n><C-w>l

" Soft Tabs
set tabstop=2
set softtabstop=2
set shiftwidth=2
set expandtab

" vim-airline config
let g:airline#extensions#tabline#enabled = 1
let g:airline#extensions#tabline#buffer_nr_show = 1

" air-line
let g:airline_powerline_fonts = 1

if !exists('g:airline_symbols')
    let g:airline_symbols = {}
endif

" unicode symbols
let g:airline_left_sep = '»'
let g:airline_left_sep = '▶'
let g:airline_right_sep = '«'
let g:airline_right_sep = '◀'
let g:airline_symbols.linenr = '␊'
let g:airline_symbols.linenr = '␤'
let g:airline_symbols.linenr = '¶'
let g:airline_symbols.branch = '⎇'
let g:airline_symbols.paste = 'ρ'
let g:airline_symbols.paste = 'Þ'
let g:airline_symbols.paste = '∥'
let g:airline_symbols.whitespace = 'Ξ'

" airline symbols
let g:airline_left_sep = ''
let g:airline_left_alt_sep = ''
let g:airline_right_sep = ''
let g:airline_right_alt_sep = ''
let g:airline_symbols.branch = ''
let g:airline_symbols.readonly = ''
let g:airline_symbols.linenr = ''

let g:ft = ''
function! NERDCommenter_before()
  if &ft == 'vue'
    let g:ft = 'vue'
    let stack = synstack(line('.'), col('.'))
    if len(stack) > 0
      let syn = synIDattr((stack)[0], 'name')
      if len(syn) > 0
        exe 'setf ' . substitute(tolower(syn), '^vue_', '', '')
      endif
    endif
  endif
endfunction
function! NERDCommenter_after()
  if g:ft == 'vue'
    setf vue
    let g:ft = ''
  endif
endfunction

